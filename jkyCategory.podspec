Pod::Spec.new do |s|
  s.name         = "jkyCategory"
  s.version      = "0.0.4"
  s.summary      = "Common Collection in iOS"
  s.homepage     = "https://bitbucket.org/matrixeray/jkycategory/overview"
  s.license      = "MIT"
  s.author       = { "matrixeray" => "june.key.young@gmail.com" }
  s.platform     = :ios, "7.0"

  s.source       = { :git => "https://matrixeray@bitbucket.org/matrixeray/jkycategory.git", :tag => s.version }


  s.source_files  = "JKYCategory/*.{h,m}"

  s.dependency "DateTools", "1.5.0"
end
