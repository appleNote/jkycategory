//
//  UIView+UtilsKit.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <QuartzCore/QuartzCore.h>
#import <objc/runtime.h>

@interface UIView (UtilsKit)

@property (nonatomic) CGFloat left;
@property (nonatomic) CGFloat right;
@property (nonatomic) CGFloat top;
@property (nonatomic) CGFloat bottom;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

- (void)showBorder;
- (void)hideBorder;

- (void)setBorderColor:(UIColor *)color;
- (void)setBorderWidth:(CGFloat)width;

#pragma mark - Round Corners

- (void)setRoundedCorners:(UIRectCorner)corners radius:(CGSize)size;

#pragma mark - Animation

/**
 * Create a shake effect on the UIView
 *
 */
- (void)shakeView;

/**
 * Create a pulse effect on th UIView
 *
 */
- (void)pulseViewWithTime:(CGFloat)seconds;



@end
