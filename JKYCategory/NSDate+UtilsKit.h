//
//  NSDate+UtilsKit.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (UtilsKit)

- (NSString *)formatTime;
- (NSString *)formatTimeWithWeekdays;
- (NSString *)formatTimeYMD;
- (NSString *)formatTimeYMDHMS;
- (NSString *)formatTimeYMDHM;
- (NSString *)formatTimeMD;

- (NSString *)formatRemainingTime;


- (BOOL)isBetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate;
- (NSInteger)distanceInDaysToDate:(NSDate *)anotherDate;


#pragma mark - English

- (NSString *)englishFormatTimeOnlyDay;
- (NSString *)englishFormatTimeMonthAndDay;
- (NSString *)englishFormatTimeMDY;
- (NSString *)englishFormatTimeMDYHM;
- (NSString *)englishFormatTimeMDYHMAmPm;

- (NSString *)englishFormatTimeMDYHMAmPmTmps;  //tmp item
- (NSString *)englishFormatTimeWeekMonthDayYear;
- (NSString *)englishFormatTimeMDYHMAmPmWithoutDot;

#pragma mark - Chinese

- (NSString *)chineseFormatTimeOnlyDay;
- (NSString *)chineseFormatTimeMonthAndDay;
- (NSString *)chineseFormatTimeMDY;
- (NSString *)chineseFormatTimeMDYHM;
- (NSString *)chineseFormatTimeMDYHMAmPm;
- (NSString *)chineseFormatTimeMDYHMAmPmTmps;  //tmp item

@end
