//
//  NSString+TextKit.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface NSString (TextKit)

/*
- (NSString *)MD5Hash;
+ (NSString *)stringWithMD5OfFile:(NSString *)path;
- (NSString *)sha1;
- (NSString *)formatBankCard;
- (NSString *)urlEncode;
- (NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
- (NSString *)urlDecode;
- (NSString *)urlDecodeUsingEncoding:(NSStringEncoding)encoding;
*/

- (NSString *)reverse;

- (NSString *)stringByStrippingWhitespace;
- (NSString *)substringFrom:(NSInteger)from to:(NSInteger)to;
- (NSString *)capitalizeFirst:(NSString *)source;
- (NSString *)underscoresToCamelCase:(NSString*)underscores;
- (NSString *)camelCaseToUnderscores:(NSString *)input;
- (NSUInteger)countWords;

- (BOOL)isEmail;
- (BOOL)isBlank;
- (BOOL)contains:(NSString *)string;


- (NSString *)removeWhitespace;
- (NSString *)removeNullString;
- (NSString *)removeAllUnderlineString;

- (NSString *)stringByStrippingHTML;

+ (BOOL)isStringEmpty:(NSString *)string;
+ (NSString *)numberTransferFormater:(NSNumber *)number;  //NSNumber to ThousandNumber  1,949

@end
