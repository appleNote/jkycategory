//
//  UINavigationBar+BottomHairline.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (BottomHairline)

- (void)hideBottomHairline;

- (void)showBottomHairline;

@end
