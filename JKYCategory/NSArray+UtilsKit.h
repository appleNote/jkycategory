//
//  NSArray+UtilsKit.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (UtilsKit)


/**
 * Return an random object from the array
 *
 */
- (instancetype)randomObject;


/**
 * Get the object at a given index in safe mode
 * Return nil if the array self is empty
 *
 */
- (instancetype)safeObjectAtIndex:(NSUInteger)index;


/**
 * Create a reversed array from self
 *
 */
- (NSArray *)reversedArray;


/**
 * Convert self to JSON as NSString
 *
 */
- (NSString *)arrayToJson;


/**
 * Convert the given array to JSON as NSString
 *
 */
+ (NSString *)arrayToJson:(NSArray *)array;


/**
 * Convert the given array to JSON as NSString
 *
 */
+ (NSArray *)reversedArray:(NSArray *)array;

@end
