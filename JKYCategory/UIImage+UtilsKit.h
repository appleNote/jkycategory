//
//  UIImage+UtilsKit.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UtilsKit)

/**
 * Apply the alpha to the current image, return the new image
 *
 */
- (UIImage *)imageByApplyingAlpha:(CGFloat)alpha;


/**
 * Scale
 */
- (UIImage *)imageCompressForSize:(CGSize)size;
- (UIImage *)imageCompressForWidth:(CGFloat)defineWidth;
+ (UIImage *)compressImageWith:(UIImage *)image;

@end
