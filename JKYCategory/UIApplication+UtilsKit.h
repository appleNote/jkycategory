//
//  UIApplication+UtilsKit.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (UtilsKit)

/**
 * Check if the device can actually make a phone call
 *
 */
- (BOOL)canMakePhoneCall;


/**
 * Start the network activity
 *
 */
+ (void)startNetworkActivity;


/**
 * Finish the network activity
 *
 */
+ (void)finishNetworkActivity;

- (void)updateNetworkActivityIndicator;


/**
 * Schedule local notification At specified time
 *
 */
- (void)scheduleLocalNotificationAtHour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second withText:(NSString *)text;


/**
 * Show the local notification now
 *
 */
- (void)showLocalNotification:(NSString *)message;


@end
