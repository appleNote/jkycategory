//
//  JKYCommonUtils.m
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import "JKYCommonUtils.h"

@implementation JKYCommonUtils

+ (BOOL)isCurrentSystemLanguageEnglish {
    NSArray *languages = [NSLocale preferredLanguages];
    NSString *currentLanguage = [languages objectAtIndex:0];
    return [currentLanguage containsString:@"en"] ? YES : NO;
}

@end
