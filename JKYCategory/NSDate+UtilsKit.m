//
//  NSDate+UtilsKit.m
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import "NSDate+UtilsKit.h"
#import "JKYCommonUtils.h"

@implementation NSDate (UtilsKit)

- (BOOL)isLanguageEnglish {
    return [JKYCommonUtils isCurrentSystemLanguageEnglish];
}

- (NSString *)formatTime {
    NSString *_timestamp;
    time_t now;
    time(&now);
    
    BOOL flag = [self isLanguageEnglish];
    
    int distance = (int)difftime(now, self.timeIntervalSince1970);
    if (distance < 0) distance = 0;
    
    if (distance < 60) {
        _timestamp = [NSString stringWithFormat:flag ? @"%ds ago" : @"%d秒前", distance];
    } else if (distance < 60 * 60) {
        distance = distance / 60;
        _timestamp = [NSString stringWithFormat:flag ? @"%dmin ago" : @"%d分钟前", distance];
    } else if (distance < 60 * 60 * 24) {
        distance = distance / 60 / 60;
        _timestamp = [NSString stringWithFormat:flag ? (distance > 1 ? @"%dhrs ago" : @"%dhr ago") : @"%d小时前", distance];
    } else if (distance < 60 * 60 * 24 * 7) {
        distance = distance / 60 / 60 / 24;
        _timestamp = [NSString stringWithFormat:flag ? (distance > 1 ? @"%ddays ago" : @"%dday ago") : @"%d天前", distance];
    } else if (distance < 60 * 60 * 24 * 7 * 4) {
        distance = distance / 60 / 60 / 24 / 7;
        _timestamp = [NSString stringWithFormat:flag ? (distance > 1 ? @"%dweeks ago" : @"%dweek ago") : @"%d周前", distance];
    } else {
        static NSDateFormatter *dateFormatter = nil;
        if (dateFormatter == nil) {
            dateFormatter = [[NSDateFormatter alloc] init];
            if (flag) {
                [dateFormatter setDateFormat:@"M月d日 HH:mm"];
            } else {
                [dateFormatter setDateFormat:@"HH:mm MM.dd"];
            }
        }
        
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
        _timestamp = [dateFormatter stringFromDate:date];
    }
    return _timestamp;
}

- (NSString *)formatTimeWithWeekdays {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yy.MM.dd"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    NSString *normalYear = [ndf stringFromDate:date];
    
    [ndf setDateFormat:@"c"];
    NSInteger weekdayIndex = [[ndf stringFromDate:date] integerValue];
    NSString *weekday;

    BOOL flag = [self isLanguageEnglish];
    switch (weekdayIndex) {
        case 1:
            weekday = flag ? @"Sun" : @"日";
            break;
        case 2:
            weekday = flag ? @"Mon" : @"一";
            break;
        case 3:
            weekday = flag ? @"Tue" : @"二";
            break;
        case 4:
            weekday = flag ? @"Wed" : @"三";
            break;
        case 5:
            weekday = flag ? @"Thr" : @"四";
            break;
        case 6:
            weekday = flag ? @"Fri" : @"五";
            break;
        case 7:
            weekday = flag ? @"Sat" : @"六";
            break;
        default:
            break;
    }
    NSString *weekWord = flag ? @"" : @"星期";
    return [NSString stringWithFormat:@"%@ %@%@", normalYear, weekWord, weekday];
}

- (NSString *)formatTimeYMD {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)formatTimeYMDHMS {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)formatTimeYMDHM {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)formatTimeMD {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    if ([self isLanguageEnglish]) {
        [ndf setDateFormat:@"MM dd"];
    } else {
        [ndf setDateFormat:@"MM月dd日"];
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)formatRemainingTime {
    NSString *_timestamp;
    time_t now;
    time(&now);
    BOOL flag = [self isLanguageEnglish];
    int distance = (int)difftime(self.timeIntervalSince1970, now);
    
    int mimute = 60;
    int hour = mimute * 60;
    int day = hour * 24;
    
    int intervaDays = distance / day;
    if (intervaDays > 0) {
        _timestamp = [NSString stringWithFormat:flag ? (intervaDays > 1 ? @"%ddays" : @"%dday") : @"%d天", intervaDays];
        
    } else if (intervaDays <= 0) {
        _timestamp = flag ? @"Expired" : @"到期";
    }
    
    return _timestamp;
}

#pragma mark - Custom

- (BOOL)isBetweenDate:(NSDate *)earlierDate andDate:(NSDate *)laterDate {
    if ([self compare:earlierDate] == NSOrderedDescending) {
        if ( [self compare:laterDate] == NSOrderedAscending ) {
            return YES;
        }
    }
    if ([self isEqual:earlierDate] || [self isEqual:laterDate]) {
        return YES;
    }
    return NO;
}

// Thanks, dmitrydims
// I have not yet thoroughly tested this
- (NSInteger)distanceInDaysToDate:(NSDate *)anotherDate {
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit fromDate:self toDate:anotherDate options:0];
    return components.day;
}


#pragma mark - English

- (NSString *)englishFormatTimeOnlyDay {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"dd"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)englishFormatTimeMonthAndDay {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"MMM dd"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)englishFormatTimeMDY {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"MMM dd,yyyy"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)englishFormatTimeMDYHM {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"MMM dd,yyyy HH:MM"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)englishFormatTimeMDYHMAmPm {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"MMM dd,yyyy hh:mm a"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)englishFormatTimeMDYHMAmPmTmps {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"MMM d,yyyy, hh:mm a"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)englishFormatTimeMDYHMAmPmWithoutDot {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"MMM d,yyyy hh:mma"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}


// Tue May 28, 2015
- (NSString *)englishFormatTimeWeekMonthDayYear {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"ccc MMM d,yyyy"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}


#pragma mark - Chinese

- (NSString *)chineseFormatTimeOnlyDay {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"dd日"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)chineseFormatTimeMonthAndDay {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"MM月dd日"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)chineseFormatTimeMDY {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yyyy年MM月dd日"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)chineseFormatTimeMDYHM {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yyyy年MM月dd日, HH:MM"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)chineseFormatTimeMDYHMAmPm {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yyyy年MM月dd日, hh:mm a"];
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.timeIntervalSince1970];
    return [ndf stringFromDate:date];
}

- (NSString *)chineseFormatTimeMDYHMAmPmTmps {
    NSDateFormatter *ndf = [[NSDateFormatter alloc] init];
    [ndf setDateFormat:@"yyyy年MM月dd日, hh:mm a"];
    NSLog(@"%@", [ndf stringFromDate:self]);
    ndf.locale = [NSLocale currentLocale];
    ndf.timeZone = [NSTimeZone localTimeZone];
    [ndf setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    return [ndf stringFromDate:self];
}


@end
