//
//  UILabel+UtilsKit.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (UtilsKit)

/**
 * Calculate the content size
 *
 */
- (CGSize)contentSize;

@end
