//
//  NSString+containsString.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (containsString)

#if __IPHONE_OS_VERSION_MIN_REQUIRED < 80000

- (BOOL)containsString:(NSString *)aString;

#endif


@end
