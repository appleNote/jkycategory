//
//  KeyerationCategories.h
//  JKYCategoryDemo
//
//  Created by June Young on 9/6/15.
//  Copyright (c) 2015 KeyerationTeam. All rights reserved.
//

#ifndef JKYCategoryDemo_KeyerationCategories_h
#define JKYCategoryDemo_KeyerationCategories_h

//It is a single header file which includes all categories in current project

#import "JKYCommonUtils.h"

#import "NSString+TextKit.h"
#import "NSString+containsString.h"
#import "UINavigationBar+BottomHairline.h"

#import "NSArray+UtilsKit.h"
#import "NSDate+UtilsKit.h"
#import "NSDictionary+UtilsKit.h"
#import "NSMutableArray+UtilsKit.h"
#import "NSNull+UtilsKit.h"
#import "UIApplication+UtilsKit.h"
#import "UIButton+UtilsKit.h"
#import "UIColor+UtilsKit.h"
#import "UIDevice+UtilsKit.h"
#import "UIImage+UtilsKit.h"
#import "UILabel+UtilsKit.h"
#import "UIView+UtilsKit.h"

#endif
